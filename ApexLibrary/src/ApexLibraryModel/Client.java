/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ApexLibraryModel;

/**
 *
 * @author dgreader
 */
public class Client {
    
    
    private int ID;
    private String clientName;     
    private String Pass;

    public Client(int ID, String clientName, String Pass) {
        this.ID = ID;
        this.clientName = clientName;
        this.Pass = Pass;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String Pass) {
        this.Pass = Pass;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
}
