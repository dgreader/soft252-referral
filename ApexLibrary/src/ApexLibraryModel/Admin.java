/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ApexLibraryModel;

/**
 *
 * @author dgreader
 */
public class Admin {
    
      
    private int ID;
    private String adminName;     
    private String Pass;

    public Admin(int ID, String adminName, String Pass) {
        this.ID = ID;
        this.adminName = adminName;
        this.Pass = Pass;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String clientName) {
        this.adminName = clientName;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String Pass) {
        this.Pass = Pass;
    }

    
    
    
    
    
}
