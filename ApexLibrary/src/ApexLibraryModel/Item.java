/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ApexLibraryModel;


    
/**
 *
 * @author domre
 */
public class Item {
    
    private String itemName;
    private int itemID;
    private int itemType;
    private boolean isLoaned;

    public Item(String itemName, int itemID, int itemType, boolean isLoaned) {
        this.itemName = itemName;
        this.itemID = itemID;
        this.itemType = itemType;
        this.isLoaned = false;
    }
    



    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public boolean isIsLoaned() {
        return isLoaned;
    }

    public void setIsLoaned(boolean isLoaned) {
        this.isLoaned = isLoaned;
    }
    
}
